TODO

* Add more states and grades in the create publication modal

* Add edit publication form
* Delete from publicationsList
* Form field validation

Post-Mortem:

* I should have merged CreatePublication and CreatePublicationForm into a single component to better handle form summision and validation.

